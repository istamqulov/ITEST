


add_kv_btn.addEventListener("click", function () {
    var add_kv_btn = document.getElementById('add_answer');
    var del_kv_btn = document.getElementById('del_answer');
    var list = document.getElementById('complex_answers');
    var role_select = document.querySelector("select.role");

   var children = list.children;
   if (children.length === 10) {
       return 0;
   }
   else{
        var new_element = children[children.length - 1].cloneNode(true);
        list.appendChild(new_element);
        var answer = new_element.querySelector("form-control");
        answer.value = "";
   }
});

del_kv_btn.addEventListener("click", function () {
        var add_kv_btn = document.getElementById('add_answer');
    var del_kv_btn = document.getElementById('del_answer');
    var list = document.getElementById('complex_answers');
    var role_select = document.querySelector("select.role");

    var children = list.children;
    if (children.length === 3) {
        return 0;
    }
    else{
        children[children.length-1].remove();
    }
});

role_select.addEventListener("change", function () {
        var add_kv_btn = document.getElementById('add_answer');
    var del_kv_btn = document.getElementById('del_answer');
    var list = document.getElementById('complex_answers');
    var role_select = document.querySelector("select.role");


    if (role_select.value === "text"){
        list.style.display = "none"
    }
    else{
        list.style.display = "block"
    }
});

