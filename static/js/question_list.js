/**
 * Created by hasan on 10/18/17.
 */

$(document).on("click", ".question-delete-link", function () {
    var url = this.getAttribute("data-url");
    var target_url = this.getAttribute("data-list-url");
    var target_elem = this.getAttribute("data-list-elem")
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);

    xhr.send();

    if (xhr.status === 200){
        ajax_get_loader(target_url, target_elem)
    }
})