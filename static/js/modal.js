/**
 * Created by hasan on 24.08.17.
 */

function ajaxModal(url, title, size, list_url, list_elem) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);

    xhr.send();

    var modal = document.getElementById("ajax_modal");
    var modal_title = modal.querySelector(".modal-title");
    var content = modal.querySelector(".modal-body");

    modal_title.innerHTML = "Подождите, идет загрузка";
    content.innerHTML = "<span class='fa fa-reload fa-spin'></span>";

    $("#ajax_modal").modal().show();

    if (xhr.status === 200){
        modal_title.innerHTML = title;
        content.innerHTML = xhr.responseText;
    }
    if (size){
        var dialog = modal.querySelector(".modal-dialog");
        if ( size == "lg" ){
            dialog.classList.add("modal-lg")
        }
    }
}

function DeleteQuestionModal(id, type){
    var modal = document.getElementById("delete_modal");
}