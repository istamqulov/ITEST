/**
 * Created by hasan on 10/21/17.
 */


function ajaxModal(url, title, size, list_url, list_elem) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);

    xhr.send();

    var modal = document.getElementById("ajax_modal");
    var modal_title = modal.querySelector(".modal-title");
    var content = modal.querySelector(".modal-body");

    modal_title.innerHTML = "Подождите, идет загрузка";
    content.innerHTML = "<span class='fa fa-reload fa-spin'></span>";

    $("#ajax_modal").modal().show();

    if (xhr.status === 200){
        modal_title.innerHTML = title;
        content.innerHTML = xhr.responseText;
    }
    if (size){
        var dialog = modal.querySelector(".modal-dialog");
        if ( size == "lg" ){
            dialog.classList.add("modal-lg")
        }
    }
}

function ajax_get_loader(url, element_selector){
    console.log(element_selector)
    element = document.querySelector(element_selector);
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send();
    if (xhr.status !== 200){
        alert('Проблема соедеинения' );
        alert(xhr.statusText);

        console.log(url)
    }
    else {
        element.innerHTML = xhr.responseText;
        element.setAttribute("data-url", url);
        element.setAttribute("data-elem", element_selector);
        element.classList.add("active");
        element.classList.add("ajax")

    }

}

function saveform(form_id) {
    var form = $(form_id);
    var formdata = form.serialize();
    var modal = $("#ajax_modal");
    $.ajax(({
        type: 'POST',
        url: form.attr("action"),
        data: formdata,
        success: function (data) {
            if (data == "ok") {
                var al =  document.querySelector("#ajaxform .alert-success");
                al.style.display = 'block';
                setTimeout(function () {
                    modal.modal("hide")
                },
                    500
                )
            }
            else{
                var al =  document.querySelector("#ajaxform .alert-danger");
                al.style.display = "block"
            }
        }
    })
    )

}

$(document).on("click", ".question-delete-link", function () {
    var url = this.getAttribute("data-url");
    var page = document.querySelector(".ajax.active");
    var data_reload_url = page.getAttribute("data-url");
    var data_reload_elem = page.getAttribute("data-elem");
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);

    xhr.send();

    if (xhr.status === 200){

        ajax_get_loader(data_reload_url, data_reload_elem)
    }
})

function numeric_filter(self, event){

    if (event.charCode >= 48 && event.charCode <= 57) return true;
    alert(this.value);
    return (event.charCode === 46 || event.charCode === 0) &&
        this.value.indexOf(String.fromCharCode(event.code)) === -1;

}