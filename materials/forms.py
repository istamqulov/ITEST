from django import forms
from .models import OneChoiceQuestion, ConformQuestion, MultipleChoiceQuestion, TextQuestion, Part, NumericalQuestion, \
    ComplexText, ComplexQuestion, OrderAsset, CSVFile


class FloatFieldWithComma(forms.FloatField):
	def clean(self, value):
		# Проверяем задано ли вообще значение
		if value:
			# Если да - заменяем запятую на точку
			value = value.replace(",",  ".")
		return super().clean(value)


class PartChangeNameForm(forms.ModelForm):
    class Meta:
        model = Part
        fields = ["name"]


class QuestionFormMixin:
    pass


class OneChoiceForm(forms.ModelForm):

    class Meta:
        model = OneChoiceQuestion
        fields = ["time", "text", "correct", "incorrect_1", "incorrect_2", "incorrect_3"]


class MultipleChoiceForm(forms.ModelForm):

    class Meta:
        model = MultipleChoiceQuestion
        fields = [
            "time",
            "text",
            "correct_1", "answer_1",
            "correct_2", "answer_2",
            "correct_3", "answer_3",
            "correct_4", "answer_4"
        ]


class TextForm(forms.ModelForm):

    class Meta:
        model = TextQuestion
        fields = [
            "time",
            "text",
            "answer"
        ]


class ConformForm(forms.ModelForm):

    class Meta:
        model = ConformQuestion
        fields = [
            "time",
            "text",
            "left_1", "right_1",
            "left_2", "right_2",
            "left_3", "right_3",
            "left_4", "right_4",
            "right_5",
            "right_6"
        ]


class NumericalForm(forms.ModelForm):
    answer_1 = FloatFieldWithComma()
    answer_2 = FloatFieldWithComma()
    allowable_error = FloatFieldWithComma()

    class Meta:
        model = NumericalQuestion
        fields = [
            "time",
            "text",
            "answer_1",
            "answer_2",
            "allowable_error"
        ]



class ComplexTextForm(forms.ModelForm):

    class Meta:
        model = ComplexText
        fields = [
            "text",
            "part"
        ]


class ComplexQuestionForm(forms.ModelForm):

    class Meta:
        model = ComplexQuestion
        fields = [
            "text",
            "time"
        ]


class ChangePartForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    part_id = forms.IntegerField(label=u'Основная категория')


class AssetForm(forms.ModelForm):
    class Meta:
        model = OrderAsset
        fields = ["file"]


class CSVUploadForm(forms.ModelForm):
    class Meta:
        model = CSVFile
        fields = [
            'csv_file',
        ]

