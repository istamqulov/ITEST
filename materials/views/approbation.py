from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views import View
from django.views.generic import TemplateView, DetailView, ListView

from materials.models import *
from .mixins import PerformerRequired

MODELS = {
    "onechoice": {
        "model": OneChoiceQuestion,
        "approbation_model": OneChoiceApprobate,
        "name": "Як чавоба",
    },
    "multiplechoice": {
        "model": MultipleChoiceQuestion,
        "approbation_model": MultipleChoiceApprobate,
        "name": "Якчанд чавоба",
    },
    "conform": {
        "model": ConformQuestion,
        "approbation_model": ConformApprobate,
        "name": "Мувофиковари",
    },
    "text": {
        "model": TextQuestion,
        "approbation_model": TextApprobate,
        "name": "Хатти",
    },
    "numerical": {
        "model": NumericalQuestion,
        "approbation_model": NumericalApprobate,
        "name": "Адади",
    },
    "complexquestion": {
        "model": ComplexQuestion,
        "approbation_model": ComplexQuestionApprobate,
        "name": "Комплекси",
    },
}


class OrderApprobationView(DetailView, PerformerRequired):
    template_name = "approbation/approbation_view.html"
    model = Order
    context_object_name = "order"
    pk_url_kwarg = 'order_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        types = {key: value for key, value in MODELS.items() }
        for type in types:
            questions = getattr(self.object, type+"s")
            approbated = self.object.approbated()[type+"s"]
            approbation_persentage = 0
            if questions.count():
                approbation_persentage = int(approbated.count()/questions.count()/0.01)
            types[type].update({
                "questions": questions,
                "approbated": approbated,
                "approbation_persentage": approbation_persentage,
            })

        context['types'] = {key: value for key, value in types.items() if value["questions"]}
        return context


class QuestionApprobationManagerView(PerformerRequired):

    def get(self, *args, **kwargs):
        order_id = self.kwargs["order_id"]
        q_type = self.kwargs["q_type"]
        m = MODELS[q_type]
        question = m["model"].objects.filter(approbated=False, part__order_id=order_id).first()
        if question:
            approbation = m["approbation_model"](question=question)
            approbation.save()
            return redirect(
                "/materials/orders/{order}/approbation/{q_type}/{approbation_id}".format(
                    order=question.part.order_id,
                    q_type=q_type,
                    approbation_id=approbation.id
                )
            )
        else:
            return redirect(reverse("order-approbation", args=[order_id]))


class QuestionApprobateView(PerformerRequired):

    def get_approbation_object(self):
        approbation_uuid = self.kwargs["id"]
        return get_object_or_404(self.approbation_model, pk=approbation_uuid)

    def get(self, *args, **kwargs):
        approbation = self.get_approbation_object()
        context = {
            'approbation': approbation,
            'shuffled': approbation.get_shuffled(),
            'question': approbation.question,
        }
        return render(self.request, self.template_name, context)

    def post(self,*args, **kwargs):
        approbation = self.get_approbation_object()
        data = self.request.POST
        self.resultate(data)
        return redirect(reverse("order-approbation-manager", args=[approbation.question.part.order_id, self.q_type]))

    def resultate(self, data):
        pass


class OneChoiceApprobateView(QuestionApprobateView):
    model = OneChoiceQuestion
    approbation_model = OneChoiceApprobate
    template_name = "approbation/one_choice_approbation.html"
    q_type = "onechoice"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True
        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = int(data.get("selected_answer") or 0)
            if approbation.match_positions[selected_answer] == 0:
                approbation.user_selected = selected_answer
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()


class MultipleChoiceApprobateView(QuestionApprobateView):
    model = MultipleChoiceQuestion
    approbation_model = MultipleChoiceApprobate
    template_name = "approbation/multiple_choice_approbation.html"
    q_type = "multiplechoice"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True

        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = int(data.get("selected_answer", 0))

            if selected_answer:
                if approbation.match_positions[selected_answer] == 0:
                    approbation.user_selected = selected_answer
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()


class ConformApprobateView(QuestionApprobateView):
    model = ConformQuestion
    approbation_model = ConformApprobate
    template_name = "approbation/conform_approbation.html"
    q_type = "conform"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True
        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = data.get("selected_answer")
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()


class TextApprobateView(QuestionApprobateView):
    model = TextQuestion
    approbation_model = TextApprobate
    template_name = "approbation/text_approbation.html"
    q_type = "text"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True
        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = data.get("selected_answer")
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()


class NumericalApprobateView(QuestionApprobateView):
    model = NumericalQuestion
    approbation_model = NumericalApprobate
    template_name = "approbation/numerical_approbation.html"
    q_type = "numerical"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True
        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = data.get("selected_answer")
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()



class ComplexQuestionApprobateView(QuestionApprobateView):
    model = ComplexQuestion
    approbation_model = ComplexQuestionApprobate
    template_name = "approbation/complexquestion_approbation.html"
    q_type = "complexquestion"

    def resultate(self, data):
        approbation = self.get_approbation_object()
        question = approbation.question
        is_invalid = data.get("is_invalid")
        question.approbated = True
        if is_invalid:
            question.accepted = False
            question.approbation_status = False
        else:
            selected_answer = data.get("selected_answer")
            question.approbation_status = True
        question.save()
        approbation.end_time = timezone.now()

        approbation.save()