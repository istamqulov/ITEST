from django.http import Http404, HttpResponse
from django.urls import reverse
from django.views.generic import ListView, View, FormView, TemplateView, DetailView, CreateView, UpdateView, \
    RedirectView, DeleteView
from django.shortcuts import render, get_object_or_404, redirect

from materials.forms import OneChoiceForm, MultipleChoiceForm, TextForm, ConformForm, NumericalForm, ComplexTextForm, \
    ComplexQuestionForm, PartChangeNameForm, AssetForm
from materials.models import Part, NumericalQuestion, ComplexText, ComplexQuestion, OrderAsset
from materials.models import Order, OneChoiceQuestion, MultipleChoiceQuestion, TextQuestion, ConformQuestion

QUESTIONS = {
    "one_choice": OneChoiceQuestion,
    "multiple_choice": MultipleChoiceQuestion,
    "text": TextQuestion,
    "conform": ConformQuestion,
    "complex_question": ComplexQuestion,
    "numerical": NumericalQuestion
}


class OrdersView(ListView):
    template_name = "orders.html"
    model = Order
    context_object_name = "orders"

    def get_queryset(self):
        return Order.objects.filter(performers=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class OrderDetailView(DetailView):
    model = Order
    context_object_name = "order"
    template_name = "order_detail.html"

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        if request.user not in order.performers.all():
            raise  Http404
        else:
            return super().get(request, *args, **kwargs)


class OrderInfoView(DetailView):
    model = Order
    context_object_name = "order"
    template_name = "order_info.html"


class PartDetailView(DetailView):
    model = Part
    context_object_name = "part"
    templates_names = ["part_detail.html", "part_complex_detail.html"]

    def get_template_names(self):
        return self.templates_names[self.object.type == "complex"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.type == "complex":
            context["texts"] = ComplexText.objects.filter(part=self.object)
            context["part_id"] = self.object.id
        return context


class PartChangeNameView(UpdateView):
    model = Part
    form_class = PartChangeNameForm
    template_name = "part_change_name.html"

    def get_success_url(self):
        return reverse("order-detail", args=[self.object.order.id])


class QuestionListView(TemplateView):
    template_name = "question_list.html"

    def get_context_data(self, part_id, **kwargs):
        request = self.request
        context =  super().get_context_data()
        part = get_object_or_404(Part, id=part_id)
        context["part"] = part
        questions = []
        questions += list(part.onechoicequestion_set.all())
        questions += list(part.multiplechoicequestion_set.all())
        questions += list(part.textquestion_set.all())
        questions += list(part.conformquestion_set.all())
        questions += list(part.numericalquestion_set.all())
        questions.sort(key=lambda x: x.position)
        context["questions"] = questions

        return context


class AbstractQuestionsListView(ListView):
    template_name = "question_list.html"
    context_object_name = "questions"
    question_type_name = ""
    question_type = ""
    add_button = False

    def get_queryset(self):
        return self.model.objects.filter(part_id=self.kwargs["part_id"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["question_type_name"] = self.question_type_name
        context["question_type"] = self.question_type
        context["add_button"] = self.add_button
        context["part"] = get_object_or_404(Part, id=self.kwargs["part_id"])
        return context


class OneChoiceQuestionsListView(AbstractQuestionsListView):
    model = OneChoiceQuestion
    question_type_name = "С одним правильным ответом"
    question_type = "one_choice"
    add_button = True


class MultipleChoiceQuestionsListView(AbstractQuestionsListView):
    model = MultipleChoiceQuestion
    question_type_name = "С несколькими правильными ответами"
    question_type = "multiple_choice"
    add_button = True


class TextQuestionsListView(AbstractQuestionsListView):
    model = TextQuestion
    question_type_name = "С текстовым ответом"
    question_type = "text"
    add_button = True


class ConformQuestionsListView(AbstractQuestionsListView):
    model = ConformQuestion
    question_type_name = "С группированием ответов"
    question_type = "conform"
    add_button = True


class NumericalQuestionListView(AbstractQuestionsListView):
    model = NumericalQuestion
    question_type_name = "С числовым ответом"
    question_type = "numerical"
    add_button = True


class ComplexQuestionListView(TemplateView):
    template_name = "complex_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        part_id = kwargs["part_id"]
        part = get_object_or_404(Part, id=part_id)
        context["part"] = part
        text = self.request.GET.get("text")
        questions = ComplexQuestion.objects.filter(part=part)
        if text:
            context["text"] = get_object_or_404(ComplexText, id=text)
            questions = questions.filter(complex_text=context["text"])
        else:
            questions = questions.filter(complex_text__isnull=True)
        context["questions"] = questions
        return context


class QuestionCreateView(CreateView):
    success_url = "/"

    def form_valid(self, form):
        form.instance.part_id = self.kwargs["part_id"]
        super().form_valid(form)
        return HttpResponse("ok")


class OneChoiceCreateView(QuestionCreateView):
    form_class = OneChoiceForm
    model = OneChoiceQuestion
    template_name = "question_add/onechoice_add.html"


class MultipleChoiceCreateView(QuestionCreateView):
    form_class = MultipleChoiceForm
    model = MultipleChoiceQuestion
    template_name = "question_add/multiplechoice_add.html"


class TextCreateView(QuestionCreateView):
    form_class = TextForm
    model = TextQuestion
    template_name = "question_add/text_add.html"


class ConformCreateView(QuestionCreateView):
    form_class = ConformForm
    model = ConformQuestion
    template_name = "question_add/conform_add.html"


class NumericalCreateView(QuestionCreateView):
    form_class = NumericalForm
    model = NumericalQuestion
    template_name = "question_add/numerical_add.html"


class ComplexTextCreateView(CreateView):
    model = ComplexText
    form_class = ComplexTextForm
    template_name = "question_add/complex_text_add.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["part_id"] = self.kwargs["part_id"]
        return context

    def get_success_url(self):
        part_id = int(self.kwargs["part_id"])
        return reverse("order-part", args=[part_id])


class ComplexQuestionCreateView(CreateView):
    form_class = ComplexQuestionForm
    model = ComplexQuestion
    template_name = "question_add/complex_add.html"

    def form_valid(self, form):
        form.instance.part_id = self.kwargs["part_id"]
        complex_text = self.request.GET.get("text", None)
        print("*"*40)
        print("form_valid")
        print(self.kwargs["part_id"])
        obj = form.save(commit=False)
        obj.set_answers(self.request.POST.getlist("answer"))
        obj.complex_text_id = complex_text
        obj.save()
        return HttpResponse("ok")


class QuestionEditView(UpdateView):
    success_url = "/"

    def form_valid(self, form):
        obj = form.save(commit=False)
        print(obj.moderation)
        if obj.moderation:
            obj.approbated = False
            obj.moderated = False
            obj.accepted = False

            obj.moderation.changed(self.request.user)

        obj.save()
        print(obj, 'saved')

        return HttpResponse("ok")


class OneChoiceEditView(QuestionEditView):
    model = OneChoiceQuestion
    form_class = OneChoiceForm
    template_name = "question_edit/onechoice_edit.html"


class MultipleChoiceEditView(QuestionEditView):
    model = MultipleChoiceQuestion
    form_class = MultipleChoiceForm
    template_name = "question_edit/multiplechoice_edit.html"


class TextEditView(QuestionEditView):
    model = TextQuestion
    form_class = TextForm
    template_name = "question_edit/text_edit.html"


class NumericalEditView(QuestionEditView):
    model = NumericalQuestion
    form_class = NumericalForm
    template_name = "question_edit/numerical_edit.html"


class ConformEditView(QuestionEditView):
    model = ConformQuestion
    form_class = ConformForm
    template_name = "question_edit/conform_edit.html"


class ComplexTextEditView(UpdateView):
    model = ComplexText
    form_class = ComplexTextForm
    template_name = "question_edit/complex_text_edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["part_id"] = self.kwargs["part_id"]
        return context

    def get_success_url(self):
        part_id = int(self.kwargs["part_id"])
        return reverse("order-part", args=[part_id])


class ComplexQuestionEditView(QuestionEditView):
    model = ComplexQuestion
    form_class = ComplexQuestionForm
    template_name = "question_edit/complex_edit.html"

    def form_valid(self, form):
        form.instance.part_id = self.kwargs["part_id"]
        obj = form.save(commit=False)
        obj.set_answers(self.request.POST.getlist("answer"))
        obj.save()
        return HttpResponse("ok")


class QuestionDeleteView(View):

    def get(self, request, *args,  **kwargs):
        question_type = kwargs["question_type"]
        pk = kwargs["pk"]
        print(pk)
        question = QUESTIONS[question_type].objects.filter(id=pk)
        question.delete()
        return HttpResponse("ok")


class ComplexTextDeleteView(View):

    def get(self, request, *args,  **kwargs):
        part_id = kwargs.get('part_id')
        complex_id = kwargs.get('pk')
        complex_text = get_object_or_404(ComplexText, id=complex_id)
        questions = complex_text.complexquestion_set
        questions.update(complex_text=None)
        complex_text.delete()
        return redirect(
            reverse(
                "order-part", args=[part_id]
            )
        )



class QuestionPreview(DetailView):
    pass


class OneChoicePreview(QuestionPreview):
    template_name = "question_preview/one_choice_preview.html"
    model = OneChoiceQuestion
    context_object_name = "question"


class OrderAssets(TemplateView):
    template_name = "assets.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        order_id = kwargs.get("order_id")
        order = get_object_or_404(Order, id=order_id)
        context["order"] = order
        context["form"] = AssetForm()
        return context

    def post(self, *args, **kwargs):
        form = AssetForm(self.request.POST, self.request.FILES)
        form = form.save(commit=False)
        form.order_id = self.kwargs["order_id"]
        form.save()
        return self.get(*args, **kwargs)


class OrderAssetDeleteView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        asset_id = self.kwargs["pk"]
        order_id = self.kwargs["order_id"]
        OrderAsset.objects.filter(pk=asset_id, order_id=order_id).delete()
        return reverse("order-assets", args=[self.kwargs['order_id']])