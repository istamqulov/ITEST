from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView, FormView, RedirectView

from .mixins import ModeratorRequired

from materials.models import Order, Part, OneChoiceQuestion, MultipleChoiceQuestion, TextQuestion, ConformQuestion, \
    NumericalQuestion, ComplexQuestion, Moderation, DefaultComment, ReviewComment

MODELS = {
    "onechoice": {
        "model": OneChoiceQuestion,
        "name": "Як чавоба",
    },
    "multiplechoice": {
        "model": MultipleChoiceQuestion,
        "name": "Якчанд чавоба",
    },
    "conform": {
        "model": ConformQuestion,
        "name": "Мувофиковари",
    },
    "text": {
        "model": TextQuestion,
        "name": "Хатти",
    },
    "numerical": {
        "model": NumericalQuestion,
        "name": "Адади",
    },
    "complexquestion": {
        "model": ComplexQuestion,
        "name": "Комплекси",
    },
}


def check_order(user, order):
    if user not in order.moderators.all():
        raise Http404


class ModerationView(TemplateView, ModeratorRequired):
    template_name = "moderator/moderation.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        request = self.request
        user = request.user
        context["orders"] = request.user.orders_for_moderate.all()
        return context


class OrderModerateView(DetailView, ModeratorRequired):
    template_name = "moderator/order_moderate.html"
    model = Order
    context_object_name = "order"
    pk_url_kwarg = 'order_id'

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        check_order(request.user, order)
        return super().get(request, *args, **kwargs)


class PartModerateView(DetailView, ModeratorRequired):
    template_name = "moderator/part_moderate.html"
    model = Part
    context_object_name = "part"
    pk_url_kwarg = 'part_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        part = context['part']
        context["questions"] = part.get_all_questions(approbated=True)
        return context


class ModerationManager(RedirectView, ModeratorRequired):

    def get_moderation_object(self, m_id):
        return Moderation.objects.get(id=m_id)

    def get_redirect_url(self, *args, **kwargs):
        m_id, request = kwargs['m_id'], self.request
        moderation = Moderation.objects.get(id=m_id)
        if request.method == "POST":
            comments = request.POST.getlist('standart-comment')
            for comment_id in comments:
                comment = DefaultComment.objects.get(id=comment_id).comment
                ReviewComment(moderation=moderation, owner='moder', comment=comment, author=request.user).save()
            moderation.reject()
        else:
            if request.GET.get('accept'):
                ReviewComment(moderation=moderation, owner='moder', comment='<span style="color:#00a65a" >Кабул шуд!</span>', author=request.user).save()
                moderation.accept()

        return reverse('part-moderate', args=[moderation.get_question().part_id])


class ModerationAddComment(ModeratorRequired):

    def post(self, request, m_id):
        comment = request.POST.get('comment') or "---"
        ReviewComment(moderation_id=m_id, comment=comment, owner=request.POST.get('owner'), author=request.user).save()
        return redirect(
            request.GET.get('next')
        )


class QuestionModerateView(DetailView, ModeratorRequired):

    template_name = 'moderator/moderation/question_moderation_view.html'
    context_object_name = 'question'

    def get_moderation(self, question):
        moderation = question.moderation
        if not moderation:
            moderation = Moderation()
            moderation.q_type = question.TYPE
            moderation.save()
            question.moderation_id = moderation.id
            print(moderation.id)
            question.save()
        return moderation

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        question = self.get_object()
        context['moderation'] = self.get_moderation(question)
        context['moderation_id'] = self.get_moderation(question).id
        context['standart_moderation_comments'] = DefaultComment.objects.all()
        return context


class OneChoiceModerateView(QuestionModerateView):
    model = OneChoiceQuestion


class MultipleСhoiceModerateView(QuestionModerateView):
    model = MultipleChoiceQuestion


class ConformModerateView(QuestionModerateView):
    model = ConformQuestion


class TextModerateView(QuestionModerateView):
    model = TextQuestion


class NumericalModerateView(QuestionModerateView):
    model = NumericalQuestion


class ComplexQuestionModerateView(QuestionModerateView):
    model = ComplexQuestion

