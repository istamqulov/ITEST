import csv

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.views.generic import FormView, CreateView

from materials.forms import CSVUploadForm
from materials.models import CSVFile


class UploadCSVView(FormView, LoginRequiredMixin):
    template_name = 'materials/csv_upload.html'
    form_class = CSVUploadForm

    def get_success_url(self):
        return "/admin/materials/order/"

    def form_invalid(self, form):
        return HttpResponseRedirect(self.get_success_url())

