from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views import View

from materials.models import Part, Order, Moderation


class PerformerRequired(View):

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        part_id = self.kwargs.get('part_id')
        order_id = self.kwargs.get('order_id')

        if part_id:
            part = get_object_or_404(Part, id=part_id)
            order = part.order

        elif order_id:
            order = get_object_or_404(Order, id=order_id)

        else:
            raise Http404

        if user not in order.performers.all():
            raise Http404
        else:
            return super().dispatch(request, *args, **kwargs)


class ModeratorRequired(View):

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        part_id = self.kwargs.get('part_id')
        order_id = self.kwargs.get('order_id')
        m_id = self.kwargs.get('m_id')

        if part_id:
            part = get_object_or_404(Part, id=part_id)
            order = part.order

        elif order_id:
            order = get_object_or_404(Order, id=order_id)
        elif m_id:
            m  = get_object_or_404(Moderation, id=m_id)
            order = m.get_question().part.order

        else:
            raise Http404

        if user not in order.moderators.all():
            raise Http404
        else:
            return super().dispatch(request, *args, **kwargs)