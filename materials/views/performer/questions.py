from django.http import  HttpResponse
from django.urls import reverse
from django.views.generic import ListView, View, TemplateView, CreateView, UpdateView, RedirectView

from django.shortcuts import get_object_or_404, redirect

from materials.forms import OneChoiceForm, MultipleChoiceForm, TextForm, ConformForm, NumericalForm,\
    ComplexQuestionForm, PartChangeNameForm
from materials.models import Part, NumericalQuestion, ComplexText, ComplexQuestion
from materials.models import OneChoiceQuestion, MultipleChoiceQuestion, TextQuestion, ConformQuestion
from ..mixins import PerformerRequired

QUESTIONS = {
    "one_choice": OneChoiceQuestion,
    "multiple_choice": MultipleChoiceQuestion,
    "text": TextQuestion,
    "conform": ConformQuestion,
    "complex_question": ComplexQuestion,
    "numerical": NumericalQuestion
}


class PartChangeNameView(UpdateView, PerformerRequired):
    model = Part
    form_class = PartChangeNameForm
    template_name = "part_change_name.html"

    def get_success_url(self):
        return reverse("order-detail", args=[self.object.order.id])


class PartQuestionListView(TemplateView, PerformerRequired):
    template_name = "part_detail.html"

    def get_context_data(self, part_id, **kwargs):
        request = self.request
        context = super().get_context_data()
        part = get_object_or_404(Part, id=part_id)
        context["part"] = part
        questions = []
        questions += list(part.onechoicequestion_set.all())
        questions += list(part.multiplechoicequestion_set.all())
        questions += list(part.textquestion_set.all())
        questions += list(part.conformquestion_set.all())
        questions += list(part.numericalquestion_set.all())
        questions.sort(key=lambda x: x.position)
        context["questions"] = questions

        return context


class PartAbstractQuestionsListView(ListView, PerformerRequired):
    template_name = "part_detail.html"
    context_object_name = "questions"
    question_type_name = ""
    question_type = ""
    add_button = False

    def get_queryset(self):
        return self.model.objects.filter(part_id=self.kwargs["part_id"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["question_type_name"] = self.question_type_name
        context["question_type"] = self.question_type
        context["add_button"] = self.add_button
        context["part"] = get_object_or_404(Part, id=self.kwargs["part_id"])
        return context


class PartOneChoiceQuestionsListView(PartAbstractQuestionsListView):
    model = OneChoiceQuestion
    question_type_name = "С одним правильным ответом"
    question_type = "one_choice"
    add_button = True


class PartMultipleChoiceQuestionsListView(PartAbstractQuestionsListView):
    model = MultipleChoiceQuestion
    question_type_name = "С несколькими правильными ответами"
    question_type = "multiple_choice"
    add_button = True


class PartTextQuestionsListView(PartAbstractQuestionsListView):
    model = TextQuestion
    question_type_name = "С текстовым ответом"
    question_type = "text"
    add_button = True


class PartConformQuestionsListView(PartAbstractQuestionsListView):
    model = ConformQuestion
    question_type_name = "С группированием ответов"
    question_type = "conform"
    add_button = True


class PartNumericalQuestionListView(PartAbstractQuestionsListView):
    model = NumericalQuestion
    question_type_name = "С числовым ответом"
    question_type = "numerical"
    add_button = True


class ComplexQuestionListView(TemplateView, PerformerRequired):
    template_name = "complex_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        part_id = kwargs["part_id"]
        part = get_object_or_404(Part, id=part_id)
        context["part"] = part
        text = self.request.GET.get("text")
        questions = ComplexQuestion.objects.filter(part=part)
        if text:
            context["text"] = get_object_or_404(ComplexText, id=text)
            questions = questions.filter(complex_text=context["text"])
        else:
            questions = questions.filter(complex_text__isnull=True)
        context["questions"] = questions
        return context


class QuestionDeleteView(RedirectView, PerformerRequired):
    def get_redirect_url(self, *args, **kwargs):
        question_type = kwargs["question_type"]
        pk = kwargs["pk"]
        print(pk)
        question = QUESTIONS[question_type].objects.filter(id=pk)
        question.delete()
        return "/materials/orders/parts/{}/questions/{}/".format(
            self.kwargs['part_id'],
            question_type,
        )


class ComplexTextDeleteView(PerformerRequired):
    def get(self, request, *args, **kwargs):
        part_id = kwargs.get('part_id')
        complex_id = kwargs.get('pk')
        complex_text = get_object_or_404(ComplexText, id=complex_id)
        questions = complex_text.complexquestion_set
        questions.update(complex_text=None)
        complex_text.delete()
        return redirect(
            reverse(
                "order-part", args=[part_id]
            )
        )


class QuestionCreateView(CreateView, PerformerRequired):
    def form_valid(self, form):
        form.instance.part_id = self.kwargs["part_id"]
        return super().form_valid(form)

    def get_success_url(self):
        return "/materials/orders/parts/{}/questions/{}/".format(
            self.kwargs['part_id'],
            self.model.TYPE,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['q_type'] = self.model.TYPE
        context['part'] = Part.objects.get(id=self.kwargs['part_id'])

        return context


class OneChoiceCreateView(QuestionCreateView):
    form_class = OneChoiceForm
    model = OneChoiceQuestion
    template_name = "questions/onechoice.html"


class MultipleChoiceCreateView(QuestionCreateView):
    form_class = MultipleChoiceForm
    model = MultipleChoiceQuestion
    template_name = "questions/multiplechoice.html"


class TextCreateView(QuestionCreateView):
    form_class = TextForm
    model = TextQuestion
    template_name = "questions/text.html"


class ConformCreateView(QuestionCreateView):
    form_class = ConformForm
    model = ConformQuestion
    template_name = "questions/conform.html"


class NumericalCreateView(QuestionCreateView):
    form_class = NumericalForm
    model = NumericalQuestion
    template_name = "questions/numerical.html"


### Question Edit Views

class QuestionEditView(UpdateView, PerformerRequired):
    success_url = "/"

    def form_valid(self, form):
        obj = form.save(commit=False)
        print(obj.moderation)
        if obj.moderation:
            obj.approbated = False
            obj.moderated = False
            obj.accepted = False

            obj.moderation.changed(self.request.user)

        obj.save()
        print(obj, 'saved')

        return redirect(self.get_success_url())

    def get_success_url(self):
        return "/materials/orders/parts/{}/questions/{}/".format(
            self.kwargs['part_id'],
            self.model.TYPE,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['edit'] = True
        context['q_type'] = self.model.TYPE
        context['part'] = self.get_object().part

        return context


class OneChoiceEditView(QuestionEditView):
    model = OneChoiceQuestion
    form_class = OneChoiceForm
    template_name = "questions/onechoice.html"


class MultipleChoiceEditView(QuestionEditView):
    model = MultipleChoiceQuestion
    form_class = MultipleChoiceForm
    template_name = "questions/multiplechoice.html"


class TextEditView(QuestionEditView):
    model = TextQuestion
    form_class = TextForm
    template_name = "questions/text.html"


class NumericalEditView(QuestionEditView):
    model = NumericalQuestion
    form_class = NumericalForm
    template_name = "questions/numerical.html"


class ConformEditView(QuestionEditView):
    model = ConformQuestion
    form_class = ConformForm
    template_name = "questions/conform.html"