from django.http import Http404, HttpResponse
from django.urls import reverse
from django.views.generic import ListView, TemplateView, DetailView, RedirectView
from django.shortcuts import render, get_object_or_404

from materials.forms import AssetForm
from materials.models import OrderAsset
from materials.models import Order
from ..mixins import PerformerRequired

class OrdersView(ListView, PerformerRequired):
    template_name = "orders.html"
    model = Order
    context_object_name = "orders"

    def get_queryset(self):
        return Order.objects.filter(performers=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class OrderDetailView(DetailView, PerformerRequired):
    model = Order
    context_object_name = "order"
    template_name = "order_detail.html"
    pk_url_kwarg = 'order_id'

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        if request.user not in order.performers.all():
            raise  Http404
        else:
            return super().get(request, *args, **kwargs)


class OrderInfoView(DetailView, PerformerRequired):
    model = Order
    context_object_name = "order"
    template_name = "order_info.html"
    pk_url_kwarg = "order_id"


class OrderAssets(TemplateView, PerformerRequired):
    template_name = "assets.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        order_id = kwargs.get("order_id")
        order = get_object_or_404(Order, id=order_id)
        context["order"] = order
        context["form"] = AssetForm()
        return context

    def post(self, *args, **kwargs):
        form = AssetForm(self.request.POST, self.request.FILES)
        form = form.save(commit=False)
        form.order_id = self.kwargs["order_id"]
        form.save()
        return self.get(*args, **kwargs)


class OrderAssetDeleteView(RedirectView, PerformerRequired):

    def get_redirect_url(self, *args, **kwargs):
        asset_id = self.kwargs["pk"]
        order_id = self.kwargs["order_id"]
        OrderAsset.objects.filter(pk=asset_id, order_id=order_id).delete()
        return reverse("order-assets", args=[self.kwargs['order_id']])