from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required

from ITEST import settings
from .views import *

performer_urlpatterns = [
    url(r"^orders/$", login_required(OrdersView.as_view()), name="orders"),
    url(r'^orders/(?P<pk>\d+)/$', OrderDetailView.as_view(), name='order-detail'),
    url(r'^orders/(?P<pk>\d+)/info$', OrderInfoView.as_view(), name='order-info'),

    url(r'^orders/parts/(?P<pk>\d+)/$', PartDetailView.as_view(), name='order-part'),
    url(r'^orders/parts/(?P<pk>\d+)/change_name$', PartChangeNameView.as_view(), name='part-change_name'),

    url(r"^orders/parts/(?P<part_id>\d+)/questions$", QuestionListView.as_view(), name="part-questions"),

    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/onechoice/$",
        OneChoiceQuestionsListView.as_view(),
        name="part-onechoices"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/multiplechoice/$",
        MultipleChoiceQuestionsListView.as_view(),
        name="part-multiplechoices"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/text/$",
        TextQuestionsListView.as_view(),
        name="part-texts"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/numerical/$",
        NumericalQuestionListView.as_view(),
        name="part-numericals"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/conform/$",
        ConformQuestionsListView.as_view(),
        name="part-conforms"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex/$",
        ComplexQuestionListView.as_view(),
        name="complex_list"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/one_choice/add/$",
        OneChoiceCreateView.as_view(),
        name="one_choice-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/multiple_choice/add/$",
        MultipleChoiceCreateView.as_view(),
        name="multiple_choice-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/text/add/$",
        TextCreateView.as_view(),
        name="text-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/numerical/add/$",
        NumericalCreateView.as_view(),
        name="numerical-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/conform/add/$",
        ConformCreateView.as_view(),
        name="conform-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex_text/add/$",
        ComplexTextCreateView.as_view(),
        name="complex_text-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex_question/add/$",
        ComplexQuestionCreateView.as_view(),
        name="complex_question-add"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/one_choice/(?P<pk>\d+)/edit/$",
        OneChoiceEditView.as_view(),
        name="one_choice-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/multiple_choice/(?P<pk>\d+)/edit/$",
        MultipleChoiceEditView.as_view(),
        name="multiple_choice-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/text/(?P<pk>\d+)/edit/$",
        TextEditView.as_view(),
        name="text-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/numerical/(?P<pk>\d+)/edit/$",
        NumericalEditView.as_view(),
        name="numerical-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/conform/(?P<pk>\d+)/edit/$",
        ConformEditView.as_view(),
        name="conform-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex_text/(?P<pk>\d+)/edit/$",
        ComplexTextEditView.as_view(),
        name="complex_text-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex_question/(?P<pk>\d+)/edit/$",
        ComplexQuestionEditView.as_view(),
        name="complex_question-edit"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/complex_text/(?P<pk>\d+)/delete/$",
        ComplexTextDeleteView.as_view(),
        name="complex_text-delete"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/(?P<question_type>\w+)/(?P<pk>\d+)/delete/$",
        QuestionDeleteView.as_view(),
        name="question-delete"
    ),
    url(
        r"^orders/parts/(?P<part_id>\d+)/questions/one_choice/(?P<pk>\d+)/preview/$",
        OneChoicePreview.as_view(),
        name="one_choice-preview"
    ),

    url(
        r"^orders/(?P<order_id>\d+)/assets/$",
        OrderAssets.as_view(),
        name="order-assets"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/assets/(?P<pk>\d+)/delete",
        OrderAssetDeleteView.as_view(),
        name="order-asset-delete"
    ),

]

moderator_urlpatterns = [
    url(
        r"^moderate/$", ModerationView.as_view(), name="moderation"
    ),
    url(
        r"^moderate/orders/(?P<pk>\d+)/$",
        OrderModerateView.as_view(),
        name="order-moderate"
    ),
    url(
        r"^moderate/orders/parts/(?P<pk>\d+)$",
        PartModerateView.as_view(),
        name="part-moderate"
    ),
    url(
        r'^moderate/moderation_manager/(?P<m_id>\d+)$',
        ModerationManager.as_view(),
        name='moderation_manager',
    ),
    url(
        r'^moderate/moderation_add_comment/(?P<m_id>\d+)$',
        ModerationAddComment.as_view(),
        name='moderation-add-comment',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/one_choice/(?P<pk>\d+)/$",
        OneChoiceModerateView.as_view(),
        name='one-choice-moderate-view',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/multiple_choice/(?P<pk>\d+)/$",
        MultipleСhoiceModerateView.as_view(),
        name='multiple-choice-moderate-view',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/conform/(?P<pk>\d+)/$",
        ConformModerateView.as_view(),
        name='conform-moderate-view',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/text/(?P<pk>\d+)/$",
        TextModerateView.as_view(),
        name='text-moderate-view',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/numerical/(?P<pk>\d+)/$",
        NumericalModerateView.as_view(),
        name='numerical-moderate-view',
    ),
    url(
        r"^moderate/orders/(?P<order_id>\d+)/complex_question/(?P<pk>\d+)/$",
        ComplexQuestionModerateView.as_view(),
        name='complexquestion-moderate-view',
    ),

]

approbation_urlpatterns = [
    url(
        r"^orders/(?P<pk>\d+)/approbation$",
        OrderApprobationView.as_view(), name="order-approbation"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/(?P<q_type>\w+)/$",
        QuestionApprobationManagerView.as_view(), name="order-approbation-manager"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/onechoice/(?P<id>[0-9a-f-]+)/$",
        OneChoiceApprobateView.as_view(), name="order-approbation-onechoice"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/multiplechoice/(?P<id>[0-9a-f-]+)/$",
        MultipleChoiceApprobateView.as_view(), name="order-approbation-multiple"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/conform/(?P<id>[0-9a-f-]+)/$",
        ConformApprobateView.as_view(), name="order-approbation-conform"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/text/(?P<id>[0-9a-f-]+)/$",
        TextApprobateView.as_view(), name="order-approbation-text"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/numerical/(?P<id>[0-9a-f-]+)/$",
        NumericalApprobateView.as_view(), name="order-approbation-numerical"
    ),
    url(
        r"^orders/(?P<order_id>\d+)/approbation/complexquestion/(?P<id>[0-9a-f-]+)/$",
        ComplexQuestionApprobateView.as_view(), name="order-approbation-complexquestion"
    ),
]


urlpatterns = performer_urlpatterns + moderator_urlpatterns + approbation_urlpatterns

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
