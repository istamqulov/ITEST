import csv
from django.core.management.base import BaseCommand, CommandError

from materials.models import CSVFile, Order, User, OrderPerformers, OrderNormative


class Command(BaseCommand):
    slug = 'disable-categories'
    help = 'Disable Categories'

    def handle(self, *args, **options):
        super(Command, self).handle(*args, **options)
        log = self.stdout.write
        log(
            self.style.SUCCESS(
                'Начала невъебического процесса генерации'
            )
        )
        objects = CSVFile.objects.filter(processed=False)
        log(
            'Найдено {} CSV файлов ожидающих обработки'
        )
        global_success = 0
        global_error = 0

        for object_ in objects:
            file_ = object_.csv_file.open('r')
            reader = csv.DictReader(file_)
            log(
                'Начинаем обработку файлика {}'.format(object_.csv_file)
            )
            file_success = 0
            file_error = 0
            for row in reader:
                try:
                    order = Order(
                        subject_id=row['subject_id'],
                        start_date=row.get('start_date', None),
                        end_date=row.get('end_date', None),
                    )
                    order.save()

                    OrderPerformers(
                        order=order,
                        performer=User.objects.get(id=row['performer_id']),
                        role='performer'
                    ).save()
                    order.moderators.add(
                        User.objects.get(id=row['moderator_id'])
                    )
                    normative = OrderNormative(
                        order=order,
                        credit=row.get('credit', 1),
                        parts_count=row.get('parts_count', 14),
                        parts_prefix=row.get('parts_prefix', 'Руз '),
                        onechoice=row.get('onechoice', 0),
                        multiplechoice=row.get('multiplechoice', 0),
                        text=row.get('text', 0),
                        conform=row.get('conform', 0),
                        numerical=row.get('numerical', 0),
                        complexquestion=row.get('complexquestion', 0),
                        type=row.get('type', 'normal')
                    ).save()
                    order.name = order.subject.name + " " + str(order.normative.credits)
                    order.save()
                    log(self.style.SUCCESS(
                        'Order {} is created'.format(order.name)
                    ))
                    file_success += 1
                except Exception as e:
                    log(
                        self.style.ERROR(
                            'Error:{} // {}'.format(str(e), row)
                        )
                    )
                    file_error += 1

                log(
                    'Обработка файла завершена. Cаксэсс - {} , Эррор - {} '.format(
                        file_success,
                        file_error
                    )
                )
                global_success += file_success
                global_error += file_error
        log(
            self.style.SUCCESS(
                'Обработка завершена всего обработо {}. Импортировано {}, не импортировано {} '.format(
                    objects.count(),
                    global_success,
                    global_error,
                )
            )
        )