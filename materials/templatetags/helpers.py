from django import template
from django.utils.safestring import mark_safe

register = template.Library()

ICONS = [
    "<span class='bg-red badge'><em class='fa fa-close'> </em></span>",
    "<span class='bg-green badge'><em class='fa fa-check'> </em></span>",
]


@register.simple_tag(name="get_type")
def get_type(question_type):

    TYPES_ICONS = {
        "one_choice": "dot-circle-o",
        "multiple_choice": "check",
        "text": "font",
        "conform": "arrows-alt",
        "numerical": "calculator",
        "complex": "columns",
        "complex_question": "columns",
    }

    return mark_safe(
        "<span class='label label-default'><em class='fa fa-lg fa-{}'> </em></span>".format(TYPES_ICONS[question_type])
    )


@register.simple_tag(name="bool_icon")
def bool_icon(boolean):

    return mark_safe(
        ICONS[boolean]
    )


@register.simple_tag(name="bool_icon_double")
def bool_icon_double(boolean_1, boolean_2):

    if not boolean_1:
        icon = "<span class='bg-gray badge' style='opacity: .3'><em class='fa fa-check'></em></span>"
    else:
        icon = ICONS[boolean_2]
    return mark_safe(icon)

