from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from materials.models.questions import *
register = template.Library()



MODELS = {
    "one_choice": {
        "model": OneChoiceQuestion,
        'template_name': "preview/onechoice_preview.html",
    },
    "multiple_choice": {
        "model": MultipleChoiceQuestion,
        'template_name': "preview/multiple_choice_preview.html",
    },
    "conform": {
        "model": ConformQuestion,
        'template_name': "preview/conform_preview.html",

    },
    "text": {
        "model": TextQuestion,
        'template_name': "preview/text_preview.html",
    },
    "numerical": {
        "model": NumericalQuestion,
        'template_name': "preview/numerical_preview.html",

    },
    "complex_question": {
        "model": ComplexQuestion,
        'template_name': "preview/complex_preview.html",
    },
}


@register.simple_tag(name="question_preview")
def question_preview(question):
    return mark_safe(
        render_to_string(
            template_name=MODELS[question.TYPE]['template_name'],
            context={
                'question': question
            }
        )
    )