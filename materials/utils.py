from .models import Order, Part


def order_compiler(orders):
    """
    :param orders: queryset 
    :return: None
    """
    for order in orders:

        parts = []

        for i in range(1, order.normative.parts_count+1):
            part = Part(
                order=order,
                name="{} {}".format(order.normative.parts_prefix, i),
                position=i
            )
            if order.normative.type == "complex":
                part.type = "complex"
            parts.append(part)

        Part.objects.bulk_create(parts)
    orders.update(compiled=True)

