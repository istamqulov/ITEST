import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from functools import reduce

from django.db.models.signals import post_save

from . import mixins
from .subjects import Subject


class BaseOrder(models.Model, mixins.TimeStampable):
    name = models.CharField(verbose_name="Наименование", max_length=255, blank=True)
    start_date = models.DateField(verbose_name="Дата начала", blank=True, null=True)
    end_date = models.DateField(verbose_name="Дата окончания", blank=True, null=True)
    active = models.BooleanField(verbose_name="Активен", default=False)
    completed = models.BooleanField(verbose_name="Выполнен", default=False)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class OrderPerformers(models.Model):
    order = models.ForeignKey("Order")
    performer = models.ForeignKey(User)
    role = models.CharField(
        max_length=15,
        choices=(
            ("author", "author"),
            ("performer", "performer")
        )
    )


class Order(BaseOrder):

    performers = models.ManyToManyField(
        User,
        through=OrderPerformers,
        related_name="orders",
        blank=True
    )

    moderators = models.ManyToManyField(
        User,
        related_name="orders_for_moderate",
        blank=True
    )

    subject = models.ForeignKey(
        Subject,
        verbose_name="Предмет"
    )

    compiled = models.BooleanField(
        default=False
    )

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    @property
    def onechoices(self):
        from materials.models import OneChoiceQuestion
        return OneChoiceQuestion.objects.filter(part__order=self)

    @property
    def multiplechoices(self):
        from materials.models import MultipleChoiceQuestion
        return MultipleChoiceQuestion.objects.filter(part__order=self)

    @property
    def conforms(self):
        from materials.models import ConformQuestion
        return ConformQuestion.objects.filter(part__order=self)

    @property
    def numericals(self):
        from materials.models import NumericalQuestion
        return NumericalQuestion.objects.filter(part__order=self)

    @property
    def texts(self):
        from materials.models import TextQuestion
        return TextQuestion.objects.filter(part__order=self)

    @property
    def complexquestions(self):
        from materials.models import ComplexQuestion
        return ComplexQuestion.objects.filter(part__order=self)

    def approbated(self):
        return {
            "onechoices": self.onechoices.filter(approbated=True),
            "multiplechoices": self.multiplechoices.filter(approbated=True),
            "conforms": self.conforms.filter(approbated=True),
            "texts": self.texts.filter(approbated=True),
            "numericals": self.numericals.filter(approbated=True),
            "complexquestions": self.complexquestions.filter(approbated=True),
        }


class OrderNormative(models.Model):
    order = models.OneToOneField(
        Order,
        related_name="normative",
    )
    credits = models.FloatField(
        default=1,
        verbose_name="Кредитов"
    )

    parts_count = models.SmallIntegerField(
        default=14
    )

    parts_prefix = models.CharField(
        max_length=20,
        default="Рӯз"
    )

    onechoice = models.SmallIntegerField(
        default=0
    )

    multiplechoice = models.SmallIntegerField(
        default=0
    )

    conform = models.SmallIntegerField(
        default=0
    )

    text = models.SmallIntegerField(
        default=0
    )

    numerical = models.SmallIntegerField(
        default=0
    )

    complexquestion = models.SmallIntegerField(
        default=0
    )

    type = models.CharField(
        max_length=15,
        choices=(("normal", "Normal"), ("complex", "complex")),
        default="normal"
    )

    class Meta:
        verbose_name = "Норматив"
        verbose_name_plural = "Нормативы"


class Part(models.Model):
    order = models.ForeignKey(
        Order,
        verbose_name="Заказ",
        related_name="parts",
    )

    name = models.CharField(
        verbose_name="Имя",
        max_length=255,
    )

    position = models.SmallIntegerField(
        blank=True,
    )

    type = models.CharField(
        max_length=15,
        default="normal",
        choices=(
            ("normal", "Обычный"),
            ("complex", "Комплекс"),
            ("level", "Уровень")
        )
    )

    class Meta:
        verbose_name = 'Часть'
        verbose_name_plural = "Части"
        ordering = ["position", "id"]

    def __str__(self):
        return self.name

    def get_all_questions(self, **filter):
        questions = [
            self.onechoicequestion_set.all(),
            self.multiplechoicequestion_set.all(),
            self.conformquestion_set.all(),
            self.numericalquestion_set.all(),
            self.textquestion_set.all(),
            self.complexquestion_set.all()
        ]
        if filter:
            questions = [x.filter(**filter) for x in questions]

        return [j for i in questions for j in i]


def get_asset_path(instance, filename):
    args = filename.split('.')
    filename, ext = "".join(args[:-1]), args[-1]
    filename = "%s.%s.%s" % (filename, str(uuid.uuid4())[:10], ext)
    return os.path.join('files/order_assets/', filename)


class OrderAsset(models.Model):
    order = models.ForeignKey(Order, related_name="assets")
    name = models.CharField(max_length=255, default="Силлабус")
    file = models.FileField(upload_to=get_asset_path)
    upload_by = models.DateTimeField(auto_now_add=True, blank=True)


class CSVFile(models.Model):
    csv_file = models.FileField(
        upload_to='csv_files/'
    )
    date = models.DateTimeField(
        auto_now_add=True
    )
    processed = models.BooleanField(
        default=False
    )


