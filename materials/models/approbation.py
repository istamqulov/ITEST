import uuid
from random import shuffle

from django.db import models
from .orders import Order
from .questions import OneChoiceQuestion, MultipleChoiceQuestion, ConformQuestion, TextQuestion, NumericalQuestion, \
    ComplexQuestion


class AbstractApprobate(models.Model):

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True
    )

    question = None

    start_time = models.DateTimeField(
        auto_now_add=True,
    )
    end_time = models.DateTimeField(
        blank=True,
        null=True,
        editable=False

    )

    class Meta:
        abstract = True

    def match(self, data):
        pass


class OneChoiceApprobate(AbstractApprobate):

    question = models.ForeignKey(
        OneChoiceQuestion,
        related_name="approbates"
    )
    match_positions = models.CharField(max_length=5, null=True, blank=True, editable=False)
    user_selected = models.SmallIntegerField(default=0)

    def match(self, data):
        selected = data["selected"]
        return int(selected == self.match_position)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if not self.match_positions:
            a = ["0", "1", "2", "3"]
            shuffle(a)
            self.match_positions = "".join(a)

    def get_shuffled(self):
        question = self.question
        answers = [
            question.correct,
            question.incorrect_1,
            question.incorrect_2,
            question.incorrect_3
        ]
        indexes = [int(x) for x in self.match_positions]
        data = {
            "text": question.text,
            'answers': [answers[indexes[i]] for i in range(4)]
        }
        return data

    def __str__(self):
        return self.question.text[:15]


class MultipleChoiceApprobate(AbstractApprobate):

    question = models.ForeignKey(
        MultipleChoiceQuestion,
        related_name="approbates"
    )
    match_positions = models.CharField(
        max_length=5,
        null=True,
        blank=True,
        editable=False
    )
    user_selected = models.CharField(
        max_length=5,
        null=True,
        blank=True,
        editable=False
    )

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if not self.match_positions:
            a = ["0", "1", "2", "3"]
            shuffle(a)
            self.match_positions = "".join(a)

    def get_shuffled(self):
        question = self.question
        answers = [
            question.answer_1,
            question.answer_2,
            question.answer_3,
            question.answer_4
        ]
        indexes = [int(x) for x in self.match_positions]
        data = {
            "text": question.text,
            'answers': [answers[indexes[i]] for i in range(4)]
        }
        return data


class ConformApprobate(AbstractApprobate):
    question = models.ForeignKey(
        ConformQuestion,
        related_name="approbates"
    )
    match_positions_left = models.CharField(max_length=5, blank=True, null=True)
    match_positions_right = models.CharField(max_length=7, blank=True, null=True)
    user_answer_positions = models.CharField(max_length=5, blank=True, null=True)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if not self.match_positions_left or not self.match_positions_right:
            tmp = ["0", "1", "2", "3", "4", "5"]
            left = tmp[:4]
            right = tmp
            shuffle(left)
            shuffle(right)
            self.match_positions_left = "".join(left)
            self.match_positions_right = "".join(right)

    def get_shuffled(self):
        question = self.question
        lefts = [
            question.left_1,
            question.left_2,
            question.left_3,
            question.left_4
        ]
        rights = [
            question.right_1,
            question.right_2,
            question.right_3,
            question.right_4,
            question.right_5,
            question.right_6
        ]
        left_indexes = [int(x) for x in self.match_positions_left]
        right_indexes = [int(x) for x in self.match_positions_right]
        data = {
            "text": question.text,
            'lefts': [lefts[left_indexes[i]] for i in range(4)],
            'rights': [rights[right_indexes[i]] for i in range(6)],
        }
        return data


class TextApprobate(AbstractApprobate):
    question = models.ForeignKey(
        TextQuestion,
        related_name="approbates"
    )
    user_answer = models.CharField(max_length=255)

    def get_shuffled(self):
        pass


class NumericalApprobate(AbstractApprobate):
    question = models.ForeignKey(
        NumericalQuestion,
        related_name="approbates"
    )
    user_answer = models.FloatField(null=True, default=None)

    def get_shuffled(self):
        pass


class ComplexQuestionApprobate(AbstractApprobate):
    question = models.ForeignKey(
        ComplexQuestion,
        related_name="approbates"
    )
    user_answer = models.FloatField(null=True, default=None)

    def get_shuffled(self):
        pass