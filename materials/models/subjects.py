from django.db import models

LANGUAGES = (
    ("tj", "Точики"),
    ("ru", "Русский"),
    ("en", "English")
)

class Subject(models.Model):

    name = models.CharField(
        verbose_name="Наименовнаие предмета",
        max_length=255
    )

    description = models.TextField(
        verbose_name="Описание",
        blank=True,
        null=True
    )

    code = models.CharField(
        verbose_name="Код предмета",
        max_length=50,
        blank=True,
        null=True
    )

    lang = models.CharField(
        verbose_name="Язык",
        max_length=10,
        choices=LANGUAGES,
        default="tj"
    )

    sync_id = models.IntegerField(
        verbose_name="ID синхронизации",
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = "Предмет"
        verbose_name_plural = "Предметы"


    def __str__(self):
        return self.name