from abc import ABC
from django.db import models


class TimeStampable():

    created = models.DateTimeField(
        verbose_name = "Добавлено",
        auto_now_add = True
    )

    updated = models.DateTimeField(
        verbose_name = "Изменено",
        auto_now = True
    )
