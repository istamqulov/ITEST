from django.contrib.auth.models import User
from django.db import models

from . import mixins


REVIEW_STATUSES = (
    ("accept", "Одобрено"),
    ("ignore", "Не одобрено"),
    ("explain", "Нужно уточнить")
)


def get_class_by_type(type_):
    from .questions import OneChoiceQuestion, MultipleChoiceQuestion, ConformQuestion, TextQuestion, NumericalQuestion, \
        ComplexText, ComplexQuestion
    QUESTION_TYPES = {
        'one_choice':  OneChoiceQuestion,
        'multiple_choice':  MultipleChoiceQuestion,
        'conform':  ConformQuestion,
        'text':  TextQuestion,
        'numerical':  NumericalQuestion,
        'complex_text':  ComplexText,
        'complex_question': ComplexQuestion,
    }
    return QUESTION_TYPES[type_]


class ReviewComment(models.Model, mixins.TimeStampable):
    time = models.DateTimeField(auto_now_add=True)
    moderation = models.ForeignKey('Moderation', related_name="comments")
    author = models.ForeignKey(User)
    comment = models.TextField()
    owner = models.CharField(
        max_length=25,
        choices=(('moder', 'Модератор'), ('performer', 'Ичрокунанда'))
    )

    def __str__(self):
        return self.comment[:80]

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"


class Moderation(models.Model, mixins.TimeStampable):
    is_open = models.BooleanField(default=True)
    q_type = models.CharField(max_length=255)

    def get_question(self):
        return get_class_by_type(self.q_type).objects.filter(moderation=self).first() #
        # вобще это хуёвая практика, но что уж поделаешь

    def accept(self):
        question = self.get_question()
        question.moderated = True
        question.accepted = True
        question.save()

    def reject(self):
        question = self.get_question()
        question.moderated = True
        question.accepted = False
        question.save()

    def changed(self, user):

        ReviewComment(moderation=self, comment='Савол тагйир дода шуд', owner='performer', author=user).save()




class DefaultComment(models.Model):
    comment = models.TextField()

    def __str__(self):
        return self.comment