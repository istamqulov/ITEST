import json
import bs4

from django.db import models

from .moderate import Moderation
from .orders import Part


class AbstractQuestion(models.Model):
    part = models.ForeignKey(
        Part,
        verbose_name="Часть"
    )
    moderation = models.OneToOneField(
        Moderation,
        blank=True,
        null=True
    )

    text = models.TextField(
        default="",
        verbose_name="Текст вопроса"
    )

    position = models.SmallIntegerField(
        default=0
    )

    time = models.SmallIntegerField(
        verbose_name="Время на ответ",
        blank=True,
        default=30
    )

    created = models.DateTimeField(
        verbose_name="Добавлено",
        auto_now_add=True,
        blank=True,
        null=True
    )

    updated = models.DateTimeField(
        verbose_name="Изменено",
        auto_now=True,
        blank=True,
        null=True
    )

    approbated = models.BooleanField(default=False)
    approbation_status = models.BooleanField(default=False)
    active = models.BooleanField(verbose_name="Активно", default=False)
    accepted = models.BooleanField(verbose_name="Одобрено", default=False)
    moderated = models.BooleanField(verbose_name="Проверено", default=False)

    class Meta:
        abstract = True
        ordering = ["position", "id"]

    def __str__(self):
        return "{} id - {} части {}".format(
            self._meta.verbose_name,
            self.id,
            self.part
        )

    def get_short_text(self):
        text = bs4.BeautifulSoup(self.text).getText()
        return text[:50]


class OneChoiceQuestion(AbstractQuestion):
    correct = models.TextField(verbose_name="Правильный ответ", blank=True, default="")
    incorrect_1 = models.TextField(verbose_name="Неправильный вариант 1", blank=True, default="")
    incorrect_2 = models.TextField(verbose_name="Неправильный вариант 2", blank=True, default="")
    incorrect_3 = models.TextField(verbose_name="Неправильный вариант 3", blank=True, default="")
    TYPE = "one_choice"

    class Meta:
        verbose_name = "Вопрос с одним правильным ответом"
        verbose_name_plural = "Вопросы с одним правильным ответом "


class MultipleChoiceQuestion(AbstractQuestion):
    answer_1 = models.TextField(verbose_name="Вариант 1", blank=True, default="")
    correct_1 = models.BooleanField(default=False)

    answer_2 = models.TextField(verbose_name="Вариант 2", blank=True, default="")
    correct_2 = models.BooleanField(default=False)

    answer_3 = models.TextField(verbose_name="Вариант 3", blank=True, default="")
    correct_3 = models.BooleanField(default=False)

    answer_4 = models.TextField(verbose_name="Вариант 4", blank=True, default="")
    correct_4 = models.BooleanField(default=False)

    TYPE = "multiple_choice"

    class Meta:
        verbose_name = "Вопрос с несколькими правильными ответами"
        verbose_name_plural = "Вопросы с несколькими правильными ответами "


class ConformQuestion(AbstractQuestion):
    """
        Знаю что структура ниже полнейший говнокод, но увы нужно было делать совместимым с остальными продуктами
    """
    left_1 = models.TextField(verbose_name="Левый", blank=True, default="")
    right_1 = models.TextField(verbose_name="Правый", blank=True, default="")

    left_2 = models.TextField(verbose_name="Левый", blank=True, default="")
    right_2 = models.TextField(verbose_name="Правый", blank=True, default="")

    left_3 = models.TextField(verbose_name="Левый", blank=True, default="")
    right_3 = models.TextField(verbose_name="Правый", blank=True, default="")

    left_4 = models.TextField(verbose_name="Левый", blank=True, default="")
    right_4 = models.TextField(verbose_name="Правый", blank=True, default="")
    right_5 = models.TextField(verbose_name="Правый", blank=True, default="")
    right_6 = models.TextField(verbose_name="Правый", blank=True, default="")

    TYPE = "conform"

    class Meta:
        verbose_name = "Вопрос с сопоставлением ответов"
        verbose_name_plural = "Вопросы с сопоставлением ответов"


class TextQuestion(AbstractQuestion):
    answer = models.TextField("Ответы")

    TYPE = "text"

    class Meta:
        verbose_name = "Вопрос с текстовым ответов"
        verbose_name_plural = "Вопросы с текстовым ответом"


class NumericalQuestion(AbstractQuestion):
    answer_1 = models.FloatField()
    answer_2 = models.FloatField(blank=True, null=True)
    allowable_error = models.FloatField(default=0.01, verbose_name="Допустимая погрешность")

    TYPE = "numerical"


class ComplexText(AbstractQuestion):

    TYPE = "complex_text"

    class Meta:
        verbose_name = "Текст вопросов с комплексными ответами"
        verbose_name_plural = "Тексты вопросов с комплексными ответами"


class ComplexQuestion(AbstractQuestion):

    complex_text = models.ForeignKey(ComplexText, blank=True, null=True)
    answers_json = models.TextField()

    TYPE = "complex_question"

    class Meta:
        verbose_name = "Комплексный вопрос"
        verbose_name_plural = "Комплексный вопрос"

    def get_answers(self):
        return json.loads(self.answers_json)

    def set_answers(self, answers):
        self.answers_json = json.dumps(answers)