from django import forms
from django.contrib import admin
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from materials.forms import ChangePartForm
from .models import *

from .utils import order_compiler


def admin_change_url(obj, tag=False):
    url = reverse(
        'admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name),
        args=[obj.id]
    )
    if not tag:
        return url
    else:
        return "<a href='{}'>{}</a>".format(url, obj)


def admin_list_url(obj, query="", tag=False, label=""):
    url = reverse(
        'admin:%s_%s_changelist' % (obj._meta.app_label, obj._meta.model_name),
    )
    url += query

    if not tag:
        return url
    else:
        return "<a href='{}'>{}</a>".format(url, label or obj)


def admin_change_urls_list(object_list):
    result = ", ".join([admin_change_url(obj, True) for obj in object_list ])
    return mark_safe(result)


def order_compile(modeladmin, request, queryset):
    order_compiler(queryset)


def order_active(modeladmin, request, queryset):
    queryset.update(active=True)

def order_unactive(modeladmin, request, queryset):
    queryset.update(active=False)

def order_completed(modeladmin, request, queryset):
    queryset.update(completed=True)


def order_uncompleted(modeladmin, request, queryset):
    queryset.update(completed=False)


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    pass


class OrderAssetInline(admin.TabularInline):
    model = OrderAsset
    extra = 0
    classes = ['collapse']
    verbose_name = "Прикрепленный материал"
    verbose_name_plural = "Прикрепленные материалы"


class PartInline(admin.TabularInline):
    model = Part
    extra = 0
    classes = ['collapse']


class OrderAdminForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ["compiled", "completed"]


class OrderPerformersInline(admin.TabularInline):
    raw_id_fields = ["performer"]
    model = OrderPerformers
    extra = 1
    verbose_name = "Исполнитель"
    verbose_name_plural = "Исполнители"


class OrderNormativeInline(admin.TabularInline):
    model = OrderNormative


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    form = OrderAdminForm
    inlines = [
        OrderPerformersInline,
        OrderAssetInline,
        OrderNormativeInline,
        PartInline,
    ]
    search_fields = ["name", "subject__name"]
    list_display = ["name", "performers_list", "start_date", "end_date", "subject", "compiled", "active", "completed"]
    list_filter = ["compiled", "start_date", "end_date", "active"]
    actions = [order_compile, order_active,order_unactive, order_completed, order_uncompleted]
    filter_horizontal = ["moderators"]

    def get_exclude(self, request, obj=None):
        if not obj:
            return ['moderators']
        else:
            return []

    def get_inline_instances(self, request, obj=None):
        if obj:
            return super().get_inline_instances(request, obj)
        else:
            return [OrderNormativeInline(self.model, self.admin_site),]

    def save_model(self, request, obj, form, change):
        if not obj.name:
            obj.name = "{} - {}".format(obj.subject.name, obj.normative.credits)

        obj.save()

    def get_queryset(self, request):
        return super().get_queryset(request).select_related().prefetch_related('performers')

    def performers_list(self, object):
        return admin_change_urls_list(object.performers.all())


# questions Admin

def set_active(modeladmin, request, queryset):
    queryset.update(active=True)


def set_accepted(modeladmin, request, queryset):
    queryset.update(accepted=True)


def move_to_part(modeladmin, request, queryset):
    form = None
    if 'apply' in request.POST:
        form = ChangePartForm(request.POST)

        if form.is_valid():
            part_id = form.cleaned_data['part_id']
            queryset.update(part_id=part_id)
            modeladmin.message_user(request, "{} вопросов перенесены на часть id = {}".format(queryset.count(), part_id))
            return redirect(request.get_full_path())

    if not form:
        form = ChangePartForm(initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

    return render(request, 'admin/move_to_part.html',
                  {'items': queryset, 'form': form, 'title': u'Изменение Части'})


class QuestionAdmin(admin.ModelAdmin):
    list_display = ["id", "get_short_text","position", "time", "part_info", "active", "accepted", "created", "updated"]
    list_display_links = ["id", "get_short_text"]
    list_filter = ["active", "accepted", "part__order__subject__name", "part__order__subject__code"]
    save_on_top = True
    actions = [set_active, set_accepted, move_to_part]

    search_fields = ['text', ]

    def part_info(self, obj):
        return mark_safe(
            "{} | {}".format(
                admin_list_url(obj, "?part__id={}".format(obj.part_id), tag=True, label=obj.part.name),
                "<strong>{}</strong>".format(obj.part_id)
            )
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('part', 'part__order', 'part__order__subject')

    def has_add_permission(self, request):
        return False


class OneChoiceAprrobateInline(admin.TabularInline):
    model = OneChoiceApprobate
    fields = ["start_time", "end_time", "match_positions", "user_selected"]
    readonly_fields = ["start_time", "end_time", "match_positions", "user_selected"]

    def has_add_permission(self, request):
        return 0


@admin.register(OneChoiceQuestion)
class OnceChoiceQuestionAdmin(QuestionAdmin):
    inlines = [OneChoiceAprrobateInline]


@admin.register(MultipleChoiceQuestion)
class MultipleQuestionAdmin(QuestionAdmin):
    pass


@admin.register(ConformQuestion)
class ConformQuestionAdmin(QuestionAdmin):
    pass


@admin.register(TextQuestion)
class TextQuestionAdmin(QuestionAdmin):
    pass

@admin.register(NumericalQuestion)
class NumericalQuestionAdmin(QuestionAdmin):
    pass


@admin.register(ComplexQuestion)
class ComplexQuestionAdmin(QuestionAdmin):
    pass

@admin.register(DefaultComment)
class ModeratorDefaultCommentAdmin(admin.ModelAdmin):
    pass
