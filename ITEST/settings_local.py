import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'f5h!^@chz!@3kas72w)_y5&qtq!5%%32o8so4icm0=($$xy^60'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "TESTDEV",
        "USER": "root",
        "PASSWORD": "1233"
    }
}

STATIC_URL = '/static/'


STATICFILES_DIRS = [os.path.join(BASE_DIR, "static/"), os.path.join(BASE_DIR, "media/")]

STATIC_ROOT = os.path.join(BASE_DIR, "static_root")

MEDIA_ROOT = os.path.join(BASE_DIR, "media/")
MEDIA_URL = "/media/"