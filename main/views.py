from django.shortcuts import render
from django.views.generic import TemplateView

from materials.models import Order


class MainPageView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        request = self.request
        context = super().get_context_data( **kwargs)
        context["page_title"] = "Index"

        return context


class IframeView(TemplateView):
    template_name = 'iframe.html'

