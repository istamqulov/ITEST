from django.conf.urls import url
from django.views.generic import RedirectView

from .views import MainPageView, IframeView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r"^$", login_required(MainPageView.as_view())),
    url(r"^iframe$", login_required(IframeView.as_view()), name='iframe-wrapper')
]