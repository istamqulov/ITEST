from django.conf.urls import url

from .views import ManagementView

urlpatterns = [
    url(r"^$", ManagementView.as_view())
]