from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, TemplateView

from materials.models import Order

class ManagementView(TemplateView):
    template_name = "management_view.html"

    def get_context_data(self, **kwargs):
        request = self.request


class OrdersView(ListView):
    model = Order
    template_name = ""




