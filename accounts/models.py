from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):

    user = models.OneToOneField(
        User,
        related_name="profile"
    )

    is_moderator = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Профиль"



class MessageManager(models.Manager):
    pass


class Message(models.Model):
    user = models.ForeignKey(User, related_name="inbox")
    sender = models.ForeignKey(User, related_name="outbox")
    send_time = models.DateTimeField(auto_now_add=True)
    text = models.TextField(verbose_name="Текст сообщения")

    class Meta:
        ordering = ["send_time"]


